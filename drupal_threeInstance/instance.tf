resource "scaleway_instance_server" "backend1" {
    name  = "${local.team}-backend1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "${local.team}", "web", "apache2" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/Files/installDrupal_Back")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

# Seconde instance

resource "scaleway_instance_server" "backend2" {
    name  = "${local.team}-backend2"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "${local.team}", "web", "apache2" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/Files/installDrupal_Back")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

resource "scaleway_instance_server" "database" {
	name  = "${local.team}-database"
	type  = "PRO2-XXS"
	image = "debian_bookworm"
	tags = [ "${local.team}", "database", "mysql" ]
	root_volume {
		size_in_gb = 10
	}
	user_data = {
	   role       = "database"
	   cloud-init = file("${path.module}/Files/installDrupal_DB")
	}
	enable_ipv6 = false
	private_network {
	   pn_id = scaleway_vpc_private_network.myvpc.id
	}
}



# Données en sortie

output "backend1_private_ip" {
  value = "${scaleway_instance_server.backend1.private_ip}"
}

output "backend2_private_ip" {
  value = "${scaleway_instance_server.backend2.private_ip}"
}

output "database_private_ip" {
  value = "${scaleway_instance_server.database.private_ip}"
}
