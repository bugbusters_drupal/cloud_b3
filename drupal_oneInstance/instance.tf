resource "scaleway_instance_ip" "bugbusters_srv1" {}

resource "scaleway_instance_server" "bugbusters_srv1" {
    name  = "bugbusters_srv1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "web", "nginx" ]
    ip_id = scaleway_instance_ip.bugbusters_srv1.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/installdrupal")
    }
  }

output "srv1_public_ip" {
  value = "${scaleway_instance_server.bugbusters_srv1.public_ip}"
}
